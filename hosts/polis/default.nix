{ pkgs, inputs, ... }:
{
    imports = [
        inputs.hardware.nixosModules.common-cpu-intel
        inputs.hardware.nixosModules.common-pc-ssd

        ./hardware-configuration.nix
        ../common/global
        ../common/optional/wireless.nix
        ../common/optional/tailscale.nix
        ../common/optional/cac.nix
        ../common/users/crepe.nix

    ];

    boot = {
        initrd.luks.devices.luksroot = {
            device = "/dev/disk/by-id/ata-WDC_WDS500G2B0B-00YS70_175142801508-part2";
            preLVM = true;                
            allowDiscards = true;
        };
        loader = {
            systemd-boot.enable = true;
            efi.canTouchEfiVariables = true;
        };
        kernelPackages = pkgs.linuxPackages_latest;
        tmpOnTmpfs = true;
    };

    programs.appgate-sdp.enable = true;
    #programs.dconf.enable = true;

    sound.enable = true;
    hardware = {
        pulseaudio = {
            enable = true;
            package = pkgs.pulseaudioFull;
        };
    };

    nixpkgs.config.pulseaudio = true;
    
    hardware.opengl = {
      enable = true;
      extraPackages = with pkgs; [
        intel-media-driver
        libvdpau-va-gl
      ];
    };
      

    services = {
        xserver = {
            libinput.enable = true;
        };
        logind = {
          lidSwitch = "suspend";
          lidSwitchExternalPower = "lock";
        };
    };
}
