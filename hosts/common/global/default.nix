{ lib, inputs, hostname, persistence, ... }:
{
    imports = [
        inputs.impermanence.nixosModules.impermanence
        ./locale.nix
        ./nix.nix
        ./openssh.nix
        ./sops.nix
    ];

    networking.hostName = hostname;

    environment = {
        persistence = lib.mkIf persistence {
            "/persist".directories = [ "/var/lib/systemd" "/var/log" ];
        };

        enableAllTerminfo = true;
    };

    #programs.fuse.userAllowOther = true;

    hardware.enableRedistributableFirmware = true;
}
