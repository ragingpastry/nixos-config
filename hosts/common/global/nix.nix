{ pkgs, ... }:
{
    nix = {
        package = pkgs.nixUnstable;
        settings = {
            sandbox = true;
            cores = 0;
            auto-optimise-store = true;
            trusted-users = [ "@wheel" "root" ];
        };
        extraOptions = ''
            experimental-features = nix-command flakes
            warn-dirty = false
        '';
        gc = {
            automatic = true;
            dates = "weekly";
        };
    };

    system.autoUpgrade = {
        enable = true;
        channel = "https://nixos.org/channels/nixos-unstable";
    };
}
