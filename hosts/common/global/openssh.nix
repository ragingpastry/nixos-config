{ persistence, outputs, lib, ... }:
let
    hosts = builtins.attrNames outputs.nixosConfigurations;
    sshPath = if persistence then "/persist/etc/ssh" else "/etc/ssh";
in
{
    services.openssh = {
        enable = true;
        passwordAuthentication = false;
        permitRootLogin = "no";
        extraConfig = ''
            StreamLocalBindUnlink yes
        '';

        hostKeys = [
            {
                bits = 4096;
                path = "${sshPath}/ssh_host_rsa_key";
                type = "rsa";
            }
            {
                path = "${sshPath}/ssh_host_ed25519_key";
                type = "ed25519";
            }
        ];
    };

    programs.ssh = {
        knownHostsFiles = lib.flatten (map
            (host: [
                ../../${host}/ssh_host_ed25519_key.pub
                ../../${host}/ssh_host_rsa_key.pub
            ])
        hosts);
    };

    security.pam.enableSSHAgentAuth = true;
}
