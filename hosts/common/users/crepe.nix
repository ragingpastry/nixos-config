{ pkgs, config, lib, hostname, outputs, ... }:
let
  homeConfig = outputs.homeConfigurations."crepe@${hostname}".config;
in
{
  users.mutableUsers = false;
  users.users.crepe = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [
      "wheel"
      "video"
      "audio"
    ]
    ++ (lib.optional config.networking.wireless.userControlled.enable config.networking.wireless.userControlled.group)
    ++ (lib.optional config.programs.wireshark.enable "wireshark")
    ++ (lib.optional config.hardware.i2c.enable "i2c")
    ++ (lib.optional config.services.mysql.enable "mysql")
    ++ (lib.optional config.virtualisation.docker.enable "docker")
    ++ (lib.optional config.virtualisation.podman.enable "podman")
    ++ (lib.optional config.virtualisation.libvirtd.enable "libvirtd");

    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDeWktAlM2xMDp8Vv2V9OT1NVUxZvbTfg3DaHn5wpjJ0rAO2XBE3JQHYdjVWebRIi2ApTmplwNIfWODaWFCGQyJ/JhYuX3aqAxgRx8+fT1VuKQZ6UWDQ0TeG3MWxL2eZpAs967Bi9vYpBecB48olNn0EEIaqPuaLUJRHpenwlcQoMo9q0uMYEmVkseOHfH4BbHdCJrlLud+OB4Lf1XNvp5uMu/9GWQchba45chQg5gCXHPnGtfcvrE9hWJCcETasazyXgoTZuU8v6fF0MpPin4gsPwJaQGvCJUWGEv3XqydGFU0db2eWLS4eioW48oY+nqG8/Z0w+jh2rubK5q9wzTVYq/EnanlBQhXoeef4TF7H2bYHwEw1pFTIUoQHS9jU0kztLF1dpIaya6228JH9ANcPAXxmRJ2YUoSuRSUJ/P/DUXLjN9WfFEx58wsuyJQJpj95hMDhg2DmOm2EmT5RQN9yfl7qLtxoYBl9jtbyI1pDl1SZB41KArGAZrpmvPw2pSdgc4D8VlxCsZMpuAftdZi15DOgcaLi+7MoHuLjbC/TQLHeioDYKFCS0p+xnUJUq/WEVxmurs/kXa0IW4krEkaYFacMsS23SMJuCkDli6kvU0YeJxb3xJMvxhny2QXJiWzGiAzHp9Gni4uZ3DmUH3yXEcSwUz+zDoP4bPLET4VdQ== nwilburn@pop-os"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC5mZAqzxW5I1Sl5Tb9njTcuqWv3xMN0IRcSbM3uvSCclwT6Ocw2pmhhhLR/l8fbS5Iy7oW84d51w5szFhIcMTYZVYx+2r4xozU6dYed2en34KLZmYEYUbA3Q1qgpjic70BnG71bphvmABtM8QY8xh82snKpnJ8/0HMCPUZSuWwfYyIH3AXCl4gQLKBbwo6cGaEMzN9ORwqgeEJa4LKXkhHgsoSppHbP+bU3rfbUNYWDxPatpvU6m2n2Qr5I2oigZPvk4pJ4Fb8rlsZ79fyQO4XmIYskYtXS+r2DE8sR52ShUEHtbSHp3mplAGR+5LYVH8q+PTSlch2ctN7Z4+n3IhB6Q0b20HF4rEAok1t7DlmdXwnyFGTrOjLHXAqqsX+WAXw/DEbqHcSxyZBeLN8ursQ1EMhhvlb1bf2RNOppxUYEA3l75lDJu0pOfFt3o82Z6FIXFbfTIINedbjR29YJoVftYJEb9M0qphgfI2+TWECf3LNb2Tk8KAlqQ91A206oDNFZWjMMfU0HDedvMulhvz+hE6pkYx5kFr17N0PUWyUYU5fYVrZWC9h7Iya9hbrpj4auoRvJQNR8wrl73/KCvkTwwSO4WwqQvBTIQjtztykGUv5jE/Nf2gHiSR9Jmaw/HsonrVjpAwH12noFt9J53n6rELuJfzsV+QKDTdnW2nmhw=="
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCpsFvqKeNhrDhtjNWLY0nStlLQNBnKWciadqjg38qK77iYyUGVZY82ut4cLzZdGW77rPSPZKw2f5AWMe0ys9tVMQXnvK5vai1q6QxYalCm2bVPGpX3xdlN7v1v29lhTRaYeLDiaPAGB0HidZkbbcZD3SCzvi9uNRdoQVrhW/UGg9LpQosPYu3SgA5Tpgw8ozn+fjD79w+32gVExCIxcCeC8qoLkKoUuFZZMWCWbFR2w16bddiaBFjRG5FYo+N+SVZVVuEf/fMzRPKN/lgtGdb83szeg3J5Jc1p9hUwnzecHZ4YITKk6CuI+Nwfvj3HSuvfmZ72iXA4FaqAd9qR+40LX297fld7STuY2MdbByxyppBub9p1FDXyW+N/zAtKfHZPtr7l9GInuIl2f9i7ZAtKWvC36CJUL9jWTEisFqT8sEkiWSrFDqhdZ9+tEvtMUjtYH2YZtbZMYHuyrYsVqBVnvkku8ndcV2s+8bjEy+zbUqRb92HArB+rJNbx9I1166USrHz+VCmUqMtcKCJnuDaFbsboRj46yJCJLJNAGCgeuxX1AxBHPh9oRQjGwNJX3yh7h2YDv6/cTBHurh/6N0uK0F7aDXKDeY8bLrK8T/OeGoq5DwKU0Jr9gjbCELho7kg/Agn5bE1UlTGhqn6RIutHXuCzx7ixj/kvtPR99ILCCw== crepe@konishi"
      "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBDXSMvByS+KG/haxosSdSFxbgFxAsO5GvD58TXIqyTYNfrByw+ebB21PA5IREfmOdlm7IViDvZooY0QL7L/3fj0= crepe@DESKTOP-VLM9M5V"
    ];
    passwordFile = config.sops.secrets.crepe-password.path;
  };

  sops.secrets.crepe-password = {
    sopsFile = ../secrets.yaml;
    neededForUsers = true;
  };
  
  security.sudo.extraRules = [
    {
      groups = [ "wheel" ];
      commands = [
        { command = "ALL"; options = [ "NOPASSWD" ]; }
      ];
    }
  ];

  services.geoclue2.enable = lib.mkDefault (
    (homeConfig.services.gammastep.enable or false) &&
    (homeConfig.services.gammastep.provider == "geoclue2")
  );
  security.pam.services = {
    swaylock = lib.mkIf homeConfig.programs.swaylock.enable { };
  };
}
