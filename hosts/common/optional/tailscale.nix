{ lib, persistence, ... }:
{
    services.tailscale.enable = true;
    networking.firewall = {
        checkReversePath = "loose";
        allowedUDPPorts = [ 41641 ];
    };

    environment.persistence = lib.mkIf persistence {
        "/persist".directories = [ "/var/lib/tailscale" ];
    };
}
