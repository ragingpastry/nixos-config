{ pkgs, config, ... }:
{
    security.pki.certificates = [ "${builtins.readFile ../files/DoD-certs.pem}" ];
    environment.systemPackages = with pkgs; [
        acsccid
        opensc
        pcsctools
        libusb
        pcsclite
    ];

    services.pcscd.enable = true;
}
