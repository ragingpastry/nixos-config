{ config, persistence, lib, ... }: {
    sops.secrets.wireless = {
        sopsFile = ../secrets.yaml;
        neededForUsers = true;
    };

    networking.wireless = {
        enable = true;

        environmentFile = config.sops.secrets.wireless.path;
        networks = {
            "Tequila House" = {
                pskRaw = "@TEQUILA_HOUSE@";
            };
            "MySpectrumWiFiC6-2G" = {
                pskRaw = "@MYSPECTRUMWIFIC6_2G@";
            };
            "Dimasx6" = {
                pskRaw = "@DIMASX6@";
            };
        };

        userControlled = {
            enable = true;
            group = "network";
        };
    };

    users.groups.network = { };

    environment.persistence = lib.mkIf persistence {
        "/persist".files = [
            "/etc/wpa_supplicant.conf"
        ];
    };
}
