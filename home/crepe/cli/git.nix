{ pkgs, ... }:
{
    programs.git = {
        enable = true;
        package = pkgs.gitAndTools.gitFull;
        userName = "Nick Wilburn";
        userEmail = "senior.crepe@gmail.com";
        extraConfig = {
            init.defaultBranch = "master";
        };
    };
}
