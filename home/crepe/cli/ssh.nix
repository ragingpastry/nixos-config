{ outputs, hostname, persistence, lib, ... }:
let
    notSelf = n: n != hostname;
    hostnames = builtins.filter notSelf (builtins.attrNames outputs.nixosConfigurations);
in
{
    programs.ssh = {
        enable = true;
    };

    home.persistence = lib.mkIf persistence {
        "/persist/home/crepe/.ssh".files = [ "known_hosts" ];
    };
}
