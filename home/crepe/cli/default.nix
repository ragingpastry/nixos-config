{ config, pkgs, ... }:
{
    imports = [
        ./git.nix
        ./ssh.nix
    ];

    home.packages = with pkgs; [
        sops
        jq
        vim
    ];

    home.preferredApps.editor = {
        cmd = config.home.preferredApps.terminal.cmd-spawn "vim";
    };

}
