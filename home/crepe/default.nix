{ inputs, lib, username, persistence, features, ... }:
{
    imports = [
        ./cli
        ./rice
        inputs.impermanence.nixosModules.home-manager.impermanence
    ]
    ++ builtins.filter builtins.pathExists (map (feature: ./${feature}) features);

    programs = {
        home-manager.enable = true;
        git.enable = true;
    };

    home = {
        inherit username;
        stateVersion = "22.05";
        homeDirectory = "/home/${username}";
        sessionVariables = {
            NIX_CONFIG = "experimental-features = nix-command flakes";
        };
        persistence = lib.mkIf persistence {
            "/persist/home/crepe" = {
                directories = [
                    "Documents"
                    "Downloads"
                    "Pictures"
                    "videos"
                ];
                #allowOther = true;
            };
        };
    };
}
