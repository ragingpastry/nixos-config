{ pkgs, ... }:
{
  imports = [
    ./discord.nix
    ./fonts.nix
    ./gtk.nix
    ./qt.nix
    ./wallpapers
  ];

  xdg.mimeApps.enable = true;
}
