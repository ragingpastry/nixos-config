{ pkgs, ... }:
{
  imports = [
    ./gammastep.nix
    ./kitty.nix
    ./mako.nix
    ./qutebrowser.nix
    ./swayidle.nix
    ./swaylock.nix
    ./waybar.nix
    ./wofi.nix
  ];

  home.packages = with pkgs; [
    imv
    mimeo
    slurp
    grim
    wf-recorder
    wl-clipboard
    wl-mirror
    ydotool
    pulseaudio
  ];

  home.sessionVariables = {
    MOZ_ENABLE_WAYLAND = true;
    QT_QPA_PLATFORM = "wayland";
    LIBSEAT_BACKEND = "logind";
  };
}
