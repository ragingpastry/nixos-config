{
  wallpaper = import ./wallpaper.nix;
  shellcolor = import ./shellcolor.nix;
  fonts = import ./fonts.nix;
  swaylock = import ./swaylock.nix;
  preferredApps = import ./preferred-apps.nix;
}
