{
    description = "My NixOS configuration";

    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
        hardware.url = "github:nixos/nixos-hardware";
        impermanence.url = "github:nix-community/impermanence";
        nur.url = "github:nix-community/NUR";
        nix-colors.url = "github:misterio77/nix-colors";

        home-manager = {
          url = "github:nix-community/home-manager";
          inputs.nixpkgs.follows = "nixpkgs";
        };

        deploy-rs = {
          url = "github:serokell/deploy-rs";
          inputs.nixpkgs.follows = "nixpkgs";
        };

        sops-nix = {
            url = "github:mic92/sops-nix";
            inputs.nixpkgs.follows = "nixpkgs";
        };

        hyprland = {
            url = "github:hyprwm/hyprland";
            inputs.nixpkgs.follows = "nixpkgs";
        };
        hyprwm-contrib = {
            url = "github:hyprwm/contrib";
            inputs.nixpkgs.follows = "nixpkgs";
        };
    };

    outputs = inputs:
        let
            lib = import ./lib { inherit inputs; };
            inherit (lib) mkSystem mkHome mkDeploys forAllSystems;
        in
        rec {
            inherit lib;

            overlays = {
                sops-nix = inputs.sops-nix.overlay;
                nur = inputs.nur.overlay;
                hyprland = inputs.hyprland.overlays.default;
                hyprwm-contrib = inputs.hyprwm-contrib.overlays.default;
            };

            homeManagerModules = import ./modules/home-manager;

            devShells = forAllSystems (system: {
                default = legacyPackages.${system}.callPackage ./shell.nix { };
            });

            apps = forAllSystems (system: rec {
                deploy = {
                    type = "app";
                    program = "${legacyPackages.${system}.deploy-rs}/bin/deploy";
                };
                default = deploy;
            });

            legacyPackages = forAllSystems (system:
                import inputs.nixpkgs {
                    inherit system;
                    overlays = builtins.attrValues overlays;
                    config.allowUnfree = true;
                }
            );

            nixosConfigurations = {
                # Laptop
                polis = mkSystem {
                    hostname = "polis";
                    pkgs = legacyPackages."x86_64-linux";
                    persistence = true;
                };
            };

            homeConfigurations = {
                # Laptop
                "crepe@polis" = mkHome {
                    username = "crepe";
                    hostname = "polis";
                    persistence = true;

                    features = [
                        "laptop"
                        "desktop/hyprland"
                    ];
                    colorscheme = "paraiso";
                    wallpaper = "M16_final.jpg";
                };
            };

            deploy = {
                nodes = mkDeploys nixosConfigurations homeConfigurations;
                magicRollback = false;
                autoRollback = false;
            };

            deployChecks = { };
        };
}
